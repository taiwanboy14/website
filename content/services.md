# MATRIX

[Matrix](https://matrix.org/) is a federated and decentralized chat protocol that supports end-to-end encryption.
- Webchat: https://ptt.hackerspaces.be
- Home server: matrix.hackerspaces.be
  
# TOOLS

There is a [YunoHost](https://yunohost.org/) instance running at [tools.hackerspaces.be](https://tools.hackerspaces.be).

Public services:
- [Pad](https://pad.hackerspaces.be)
- [Poll](https://poll.hackerspaces.be)
- [Paste](https://paste.hackerspaces.be)
- [Drop](https://drop.hackerspaces.be)

Private services (_requires an account_):
- [Nextcloud](https://cloud.hackerspaces.be)
- [Mail](https://tools.hackerspaces.be/webmail/)

[Status](https://status.hackerspaces.be/) of the services.

# CONTACT
Matrix room: `#hsbe:hackerspaces.be`

These services follow the values defined by the [Librehosters network](https://libreho.st/).